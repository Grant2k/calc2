let clear = () => {
     $(".screen").text("0").css('font-size', "60px");
     expression = "";
     console.log ("expression = " + expression); 
}

let toCalculate = () => {
     expression = $(".screen").text();
     console.log("expression= " + expression);
     let result = eval(expression);
     $(".screen").empty();
     $(".screen").append(result); 
     $("#scale").quickfit({max: 60, min: 8}); 
     console.log ("result = " + result);
     expression = result;
}

function isValidNumber(num) {
     return !isNaN(parseFloat(num)) && isFinite(num);
}
//include 