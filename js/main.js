const PLUS = "+";
const MINUS = "-";
const MULTIPLY = "*";
const DIVIDE = "/";
const DEL = "del";
const EQUAL = "=";
const DECIMAL = ".";
let a = "";
let b = "";
let op = "";
let expression = "";

$( document ).ready(function() {
     $(".onscreen").click(function() {
               let value = $(this).attr("value");
               let screen = $(".screen"); 
               let display = screen.text();
               let decimalDot = isDecimalDot(value);

               if (display == "0") {
                    screen.empty();
                    if (decimalDot) {
                    value = `0${value}`;
                    }
               }
               
               screen.append(value); 
               //expression += value;
               console.log("exp= " + expression);     
               $("#scale").quickfit({max: 60, min: 8}); 
               
          });

     $(".delete").click(function() {  
          clear();
     });

     $(".equally").click(function() {
     toCalculate();
     });

     $(function(){
          $("#rsz").resizable();
     });
     
});

function isDecimalDot (value) {
     return value == DECIMAL;
}